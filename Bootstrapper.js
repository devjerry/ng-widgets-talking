
glbLive.Bootstrapper = function() {

	var public = {}
	public.addIframe = addIframe

	function addIframe(outerConfigName, innerConfigName) {
		var vv = {}
		//	Localize config objects
		{
			vv.outerConfig = window[outerConfigName]
			vv.innerConfig = window[innerConfigName]
		}
		//	Ref parent elm.
		{
			vv.parentEl = document.getElementById(vv.outerConfig.iframeId+"Wrap")
		}
		//	Create child (naked) iframe.
		{
			vv.iframe = document.createElement('iframe')
			vv.iframe.setAttribute('id', vv.outerConfig.iframeId)
			vv.iframe.setAttribute('frameborder', '0')
			vv.iframe.setAttribute('scrolling', 'no')
			vv.iframe.setAttribute('src', 'about:blank')
			vv.iframe.setAttribute('data-outer_config_name', outerConfigName)
			vv.iframe.setAttribute('data-inner_config_name', innerConfigName)
		}
		//	Append iframe to parent.
		{
			vv.parentEl.appendChild(vv.iframe)
		}
		//	Inject content into iframe.
		{
			vv.iframeDoc = vv.iframe.contentWindow.document
			vv.iframeCoreHTML = generatePage(vv.innerConfig, vv.iframe)
			console.dir(vv.iframeCoreHTML)
			vv.iframeDoc.write(vv.iframeCoreHTML)
			vv.iframeDoc.close()
		}
		//	Create ref to iframe in named global scope.
		{
			glbLive[vv.outerConfig.iframeId] = vv.iframe
		}
	}
	
	function generatePage(innerConfig) {
		//	Define template.
		{
			var theCoreHTML =''
				+'<!DOCTYPE html>'
				+'<!-- START - HTML-INJECTION -->'
				+'[-TheInjections-]'
				+'<!-- END - HTML-INJECTION -->'
				+'</html>'
		}
		//	Replace placeholders with css and js blocks (start from bottom).theHtmlAsDocWriteJsLocs
		{		
			theCoreHTML = theCoreHTML.replace('[-TheInjections-]', buildInjectionsBlock(innerConfig.scriptInjections) )
		}
		///console.log("The core html: ", theCoreHTML)
		return theCoreHTML;
	}


	function buildInjectionsBlock(scriptInjections) {
		vv = {}
		vv.scriptInjections = scriptInjections
		vv.scriptInjectionsLen = vv.scriptInjections.length
		console.log(vv.scriptInjections)
		vv.block = ""
		for (vv.idx = 0; vv.idx < vv.scriptInjectionsLen; vv.idx++) {
			vv.scriptInjection = vv.scriptInjections[vv.idx]
			console.log(vv.scriptInjection)
			vv.scriptInjectionName = vv.scriptInjection[0]
			vv.scriptInjectionType = vv.scriptInjection[1]
			if (vv.scriptInjectionType === "css") {
				 vv.block = vv.block + "<li"+"nk href='" + vv.scriptInjectionName + "' rel='stylesheet' type='text/css'>"
			}
			else if (vv.scriptInjectionType === "js") {
				 vv.block = vv.block + "<scr"+"ipt src='" + vv.scriptInjectionName + "'></scr"+"ipt>"
			}
		}
		return vv.block
	}

	return public
}
