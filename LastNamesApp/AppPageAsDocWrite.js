//	INLINE ROUTINES:
{
	//	Write out (further) body content.
	{
var strVar="";
strVar += "<html ng-app=\"LastNamesApp\">";
strVar += "<head>";
strVar += "<\/head>";
strVar += "";
strVar += "<script>";
strVar += "	glbLive = {};";
strVar += "	glbLive.reverseNames = function() {";
strVar += "		angular.element(thePrimaryControllerTag).scope().methods.pubReverseNames();";
strVar += "	};";
strVar += "	glbLive.addName = function() {";
strVar += "		var nameToAdd = \"Hopkins_\"+Date.now();";
strVar += "		angular.element(thePrimaryControllerTag).scope().methods.pubAddName(nameToAdd);";
strVar += "	};";
strVar += "<\/script>";
strVar += "";
strVar += "<body>";
strVar += "	=== Last Names App ===";
strVar += "	<br><br>";
strVar += "	<button onclick=\"glbLive.reverseNames()\">Reverse Names (outside of ng-app)<\/button> ";
strVar += "	<button onclick=\"glbLive.addName()\">Add Name (outside of ng-app)<\/button>";
strVar += "	<button onclick=\"parent.window.glbLive.Widget1.contentWindow.glbLive.pubScope.methods.pubReverseNames();\">Reverse First Names (in other widget)<\/button>";
strVar += "	<button onclick=\"parent.window.glbLive.Widget1.contentWindow.glbLive.addName();\">Add First Name (in other widget)<\/button>";
strVar += "	<br><br>";
strVar += "	<div>";
strVar += "		<script src=\"\/\/code.angularjs.org\/1.3.0-beta.17\/angular.js\"><\/script>	 ";
strVar += "	 ";
strVar += "		<div id=\"thePrimaryControllerTag\" ng-controller=\"NamesController\" class=\"ng-scope\">";
strVar += "			 Name:<input type=\"text\" ng-model=\"datas.newName\" class=\"ng-pristine ng-valid\">";
strVar += "			<button ng-click=\"methods.addName()\">Add<\/button>";
strVar += "			<h2>{{label}}<\/h2>";
strVar += "			<ul>";
strVar += "				<li ng-repeat=\"name_ in datas.names\"> {{name_}} <\/li>";
strVar += "			<\/ul>";
strVar += "		<\/div>";
strVar += "		";
strVar += "";
strVar += "		<script>";
strVar += "			angular.module(\"LastNamesApp\", [])";
strVar += "				.controller(\"NamesController\", function($scope, $window) {";
strVar += "					$scope.datas = {};";
strVar += "					$scope.methods = {};";
strVar += "					$scope.label = \"Names:\";";
strVar += "					$scope.datas.names = [\"Jones\", \"Smith\"];";
strVar += "";
strVar += "					$scope.methods.addName = function(){";
strVar += "						$scope.datas.names.push($scope.datas.newName);";
strVar += "						console.log(\"$scope.methods.addName - datas.names: \", $scope.datas.names);";
strVar += "					};";
strVar += "					";
strVar += "					$scope.methods.pubReverseNames = function(){";
strVar += "						$scope.datas.names = $scope.datas.names.reverse();";
strVar += "						$scope.$apply();";
strVar += "						console.log(\"$scope.methods.pubReverseNames - datas.names: \", $scope.datas.names);";
strVar += "					};";
strVar += "					";
strVar += "					$scope.methods.pubAddName = function(name){";
strVar += "						$scope.datas.names.push(name);";
strVar += "						$scope.$apply();";
strVar += "						console.log(\"$scope.methods.addName2 - datas.names: \", $scope.datas.names);";
strVar += "					};";
strVar += "					";
strVar += "					$scope.$watch(function() { return $scope.datas.names }, function() { console.log(\"watch $scope.datas.names \/ show glbLive.theNames: \", glbLive.theNames) }, true);";
strVar += "";
strVar += "					glbLive.pubScope = angular.element(thePrimaryControllerTag).scope();";
strVar += "				} );";
strVar += "		<\/script>";
strVar += "	<\/div>";
strVar += "<\/body>";


		document.write(strVar)
	}
	
	
	//	Content should be layed out on page, so run whenLoaded().
	{
		whenLoaded();
	}
}


//	HELPER FUNCTIONS LIBRARY:

function whenLoaded() {
	var vv = {}

	//	What we define as steps.
	{
		// Snap iframe height to content height.
		{
			vv.height = document.body.scrollHeight
			if (vv.height > 10) {
				vv.height = vv.height + 20
			}
			window.frameElement.style.height = vv.height + 'px'
		}
	}

	//	Do what client defines as steps.
	{
		var outer_config_name = window.frameElement.getAttribute('data-outer_config_name')
		parent.window[outer_config_name].whenLoadedFnc()
	}
}



